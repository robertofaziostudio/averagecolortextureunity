﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AverageColors : MonoBehaviour
{
    public Text coloreMedio;
    public Text coloreMedioHEX;
    public Text testo;
    public RawImage quad;
    Texture texture;
    Color[] colors;
    int[] pixels;
    ushort w = 600;
    ushort h = 600;
    Color average;
    byte[] imageBytes;

    Color getAverageColor(ushort w, ushort h)
    {
        pixels = new int[w * h];
        colors = new Color[pixels.Length];
        float r = 0;
        float g = 0;
        float b = 0;

        string path = Path.Combine(Application.streamingAssetsPath, "Image/img.jpg");
        Debug.Log(path);
        Texture2D t;
        if (File.Exists(path))
        {
            Debug.Log("Exist");
            imageBytes = File.ReadAllBytes(path);
        }
        t = new Texture2D(w, h);
        t.LoadImage(imageBytes);

        Debug.Log(t.ToString());
        texture = t;
        gameObject.GetComponent<RawImage>().texture = t;

        Texture2D src = new Texture2D(w, h, TextureFormat.ARGB32, false);
        // Convert Texture to Texture 2D
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture renderTexture = new RenderTexture(w, h, 32);
        Graphics.Blit(texture, renderTexture);
        RenderTexture.active = renderTexture;

        src.ReadPixels(new Rect(0, 0, w, h), 0, 0);
        src.Apply();

        for (int x = 0; x < w; x++)
        {
            for (int y = 0; y < h; y++)
            {
                int index = x * w + y;

                colors[index] = src.GetPixel(x, y);
                //Debug.Log(colors[index]);

                r += colors[index].r;
                g += colors[index].g;
                b += colors[index].b;
            }
        }

        average = new Color(r / colors.Length, g / colors.Length, b / colors.Length);
        Debug.Log("Average Color RGB: " + average + " #" + ColorUtility.ToHtmlStringRGB(average));
        return average;
    }

    void Start()
    {
        getAverageColor( w, h);

        coloreMedio.text = average.ToString();
        coloreMedioHEX.text = "#" + ColorUtility.ToHtmlStringRGB(average);
        testo.color = average;
        quad.color = average;
    }
}
